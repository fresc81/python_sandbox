#!/bin/python

# zufallsgenerator
import random

# wurzel
from math import sqrt

from socket import *
addr = ("", 4711)
buf  = 4096

UDPSock = socket(AF_INET, SOCK_DGRAM)
UDPSock.bind(addr)

while 1:
  data, addr = UDPSock.recvfrom(buf)
  if data:
    print "Client:", data
    eingabe = int(data)
    data = ""
    
    # ergebnis der wuerfe
    anzahl = [ 0, 0, 0, 0, 0, 0 ]

    # berechnung der schranken
    n = eingabe / 6
    schrankeUnten = n - sqrt(n)
    schrankeOben = n + sqrt(n)

    # einhaltung der schranken ueberpruefen
    def validieren(anzahl):
        return (anzahl >= schrankeUnten) and (anzahl <= schrankeOben) 

    # wuerfeln
    for x in range(eingabe):
        wurf = random.randint(0, 5)
        anzahl[wurf] += 1

    # ergebnis ausgeben
    for i in range(6):
        if validieren(anzahl[i]):
            valide = "ja"
        else:
            valide = "nein"
        data += "Anzahl der " + str(i+1) + "er: " + str(anzahl[i]) + " ( gleichverteilt " + valide + ")\n"
    
    UDPSock.sendto(data, addr)
  else:
    break
    
UDPSock.close()
