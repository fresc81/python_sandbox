#!/bin/python

import sys
from socket import *

addr = ("127.0.0.1", 4711)
buf = 4096

UDPSock = socket(AF_INET, SOCK_DGRAM)

data = str(int(sys.argv[1]))
UDPSock.sendto(data, addr)
data, addr = UDPSock.recvfrom(buf)
print data

UDPSock.close()
