#!/bin/python

# zufallsgenerator
import random

# wurzel
from math import sqrt

# anzahl eingeben
eingabe = int(input("Wie oft wuerfeln? "))

# ergebnis der wuerfe
anzahl = [ 0, 0, 0, 0, 0, 0 ]

# berechnung der schranken
n = eingabe / 6
schrankeUnten = n - sqrt(n)
schrankeOben = n + sqrt(n)

# einhaltung der schranken ueberpruefen
def validieren(anzahl):
    return (anzahl >= schrankeUnten) and (anzahl <= schrankeOben) 

# wuerfeln
for x in range(eingabe):
    wurf = random.randint(0, 5)
    anzahl[wurf] += 1

# ergebnis ausgeben
for i in range(6):
    if validieren(anzahl[i]):
        valide = "  ja"
    else:
        valide = "nein"
    print "Anzahl der", i+1, "er:", anzahl[i], "( gleichverteilt", valide, ")"
