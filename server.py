#!/bin/python

from socket import *
addr = ("", 4711)
buf  = 128

UDPSock = socket(AF_INET, SOCK_DGRAM)
UDPSock.bind(addr)

while 1:
  data, addr = UDPSock.recvfrom(buf)
  if data:
    print "Client:", data
    data=str(float(data)*2)
    UDPSock.sendto(data, addr)
  else:
    break
    
UDPSock.close()
