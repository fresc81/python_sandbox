#!/bin/python

import sys
from socket import *

addr = ("127.0.0.1", 4711)
buf = 128

UDPSock = socket(AF_INET, SOCK_DGRAM)

data = str(float(sys.argv[1]))
UDPSock.sendto(data, addr)
data, addr = UDPSock.recvfrom(buf)
print "Antwort:", data

UDPSock.close()
